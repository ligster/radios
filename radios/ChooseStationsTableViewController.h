//
//  ChooseStationsTableViewController.h
//  radios
//
//  Created by Artem on 27.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseStationsTableViewController : UITableViewController

@property (nonatomic,strong) NSMutableArray *radioStations;
@property (nonatomic,strong) NSString *id;

@end
