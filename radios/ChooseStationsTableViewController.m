//
//  ChooseStationsTableViewController.m
//  radios
//
//  Created by Artem on 27.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import "ChooseStationsTableViewController.h"
#import "Stations.h"
#import "PlayerViewController.h"

@interface ChooseStationsTableViewController ()

@end

@implementation ChooseStationsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    Stations *stations = [Stations new];
    [stations DowlonadStationsWithId:self.id CompletionBlock:^{
    self.radioStations = stations.stations[@"stations"][@"response"][@"data"][@"stationlist"][@"station"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PlayerSegue"])
    {
        PlayerViewController  *playerController = [segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        playerController.idStation = [NSString stringWithFormat:@"%@", self.radioStations[indexPath.row][@"id"]];
        playerController.detailsDict = self.radioStations[indexPath.row];
    }
}


#pragma mark - @protocol UITableViewDataSource<NSObject>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int number = [self.radioStations count];
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"stationCell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:@"stationCell"];
    }
    cell.textLabel.text = self.radioStations[indexPath.row][@"name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", self.radioStations[indexPath.row][@"id"]];
    return cell;
}


#pragma mark - @protocol UITableViewDelegate<NSObject, UIScrollViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"PlayerSegue" sender:nil];
}

@end
