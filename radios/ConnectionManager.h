//
//  ConnectionManager.h
//  radios
//
//  Created by Artem on 25.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionManager : NSObject

+ (ConnectionManager *)sharedInstance;
+ (void)DownloadData:(NSString *)url completionBlock:(void(^)(NSDictionary*))complete;
+ (void)DownloadDataToPlay:(NSString *)url completionBlock:(void(^)(NSData*))complete;

@end
