//
//  ConnectionManager.m
//  radios
//
//  Created by Artem on 25.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import "ConnectionManager.h"

@implementation ConnectionManager
static NSString *apiKey = @"fa1669MuiRPorUBw";

+ (ConnectionManager *)sharedInstance
{
    static ConnectionManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ConnectionManager alloc] init];
    });
    return sharedInstance;
}
+ (void)DownloadData:(NSString *)url completionBlock:(void(^)(NSDictionary*))complete
{
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *jsonData =
    [session dataTaskWithURL:[NSURL URLWithString:url]
           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
               if (data)
               {
                   NSDictionary *stations = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                   complete(stations);
               }
               else
               {
                   complete(nil);
               }
           }];
    [jsonData resume];
}

+ (void)DownloadDataToPlay:(NSString *)url completionBlock:(void(^)(NSData*))complete
{
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *jsonData =
    [session dataTaskWithURL:[NSURL URLWithString:url]
           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
               if (data)
               {
                   NSData *station = [[NSData alloc] initWithData:data];
                   complete(station);
               }
               else
               {
                   complete(nil);
               }
           }];
    [jsonData resume];
    
}

@end
