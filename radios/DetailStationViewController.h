//
//  DetailStationViewController.h
//  radios
//
//  Created by Artem on 31.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailStationViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *genre;
@property (strong, nonatomic) IBOutlet UILabel *bitreate;
@property (strong, nonatomic) IBOutlet UILabel *ct;
@property (strong,nonatomic) NSDictionary *detailsDict;

@end
