//
//  DetailStationViewController.m
//  radios
//
//  Created by Artem on 31.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import "DetailStationViewController.h"

@interface DetailStationViewController ()

@end

@implementation DetailStationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.name.text = self.detailsDict[@"name"];
    self.genre.text = self.detailsDict[@"genre"];
    self.ct.text = self.detailsDict[@"ct"];
    self.bitreate.text = self.detailsDict[@"bit"];
}

@end
