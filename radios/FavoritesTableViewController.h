//
//  FavoritesTableViewController.h
//  radios
//
//  Created by Artem on 01.04.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoritesTableViewController : UITableViewController

@property (nonatomic,retain) NSArray *favoritesArray;

@end
