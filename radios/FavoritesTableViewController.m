//
//  FavoritesTableViewController.m
//  radios
//
//  Created by Artem on 01.04.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import "FavoritesTableViewController.h"
#import "PlayerViewController.h"

@interface FavoritesTableViewController ()

@end

@implementation FavoritesTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = paths[0];
    NSString *stationsCacheFilename = [documentsPath stringByAppendingPathComponent:@"favorites.plist"];
    _favoritesArray = [NSArray arrayWithContentsOfFile:stationsCacheFilename];
    [self.tableView reloadData];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PlayFavoriteSegue"])
    {
        PlayerViewController  *playerController = [segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        playerController.idStation = [NSString stringWithFormat:@"%@", self.favoritesArray[indexPath.row][@"id"]];
        playerController.detailsDict = self.favoritesArray[indexPath.row];
    }
}


#pragma mark - @protocol UITableViewDataSource<NSObject>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int number = [self.favoritesArray count];
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FavoriteCell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:@"FavoriteCell"];
    }
    
    cell.textLabel.text = [[self.favoritesArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", self.favoritesArray[indexPath.row][@"id"]];
    return cell;
}


#pragma mark - @protocol UITableViewDelegate<NSObject, UIScrollViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self performSegueWithIdentifier:@"PlayFavoriteSegue" sender:nil];
}

@end
