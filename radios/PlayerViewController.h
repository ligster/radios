//
//  PlayerViewController.h
//  radios
//
//  Created by Artem on 31.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface PlayerViewController : UIViewController

- (IBAction)addToFavorites:(id)sender;
- (IBAction)details:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *metaInfo;
@property (nonatomic,strong) NSString *idStation;
@property (nonatomic,strong) MPMoviePlayerController *radioPlayer;
@property (nonatomic,strong) NSURL *streamString;
@property (nonatomic,strong) NSString *metadataInfo;
@property (nonatomic,strong) UILabel *label;
@property (nonatomic,strong) NSDictionary *detailsDict;
@property (nonatomic,strong) NSUserDefaults *userDefaults;
@property (nonatomic,strong) NSMutableArray *favoritesArray;

@end
