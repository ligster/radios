//
//  PlayerViewController.m
//  radios
//
//  Created by Artem on 31.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import "PlayerViewController.h"
#import "Stations.h"
#import "DetailStationViewController.h"

@interface PlayerViewController ()

@end

@implementation PlayerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    Stations *stations1 = [Stations new];
    [stations1 DownloadStationToPlayWithId:self.idStation CompletionBlock:^{
        NSURL *url = [[NSURL alloc] initWithString:stations1.radioStation];
        self.streamString = url;
        NSLog(@"%@",self.streamString);
        dispatch_async(dispatch_get_main_queue(), ^{
        _radioPlayer = [[MPMoviePlayerController alloc] initWithContentURL:self.streamString];
        [_radioPlayer.view setFrame:CGRectMake(0, 0, 320, 320)];
        _radioPlayer.movieSourceType = MPMovieSourceTypeStreaming;
        _radioPlayer.fullscreen = YES;
        _radioPlayer.controlStyle = MPMovieControlStyleDefault;
        [self.view addSubview:_radioPlayer.view];
        [_radioPlayer prepareToPlay];
        [_radioPlayer play];
        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(MetadataUpdate:)
            name:MPMoviePlayerTimedMetadataUpdatedNotification
            object:nil];
        });
        
    }];
}

- (void)MetadataUpdate:(NSNotification *)notification
{
    if ([_radioPlayer timedMetadata] != nil && [[_radioPlayer timedMetadata] count] > 0)
    {
        MPTimedMetadata *firstMeta = _radioPlayer.timedMetadata[0];
        self.metadataInfo = firstMeta.value;
        self.metaInfo.text = self.metadataInfo;
        NSLog(@"%@",self.metadataInfo);
    }
}

- (IBAction)addToFavorites:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = paths[0];
    NSString *stationsCacheFilename = [documentsPath stringByAppendingPathComponent:@"favorites.plist"];
    _favoritesArray = [NSMutableArray arrayWithContentsOfFile:stationsCacheFilename];
    [_favoritesArray addObject:self.detailsDict];
    [_favoritesArray writeToFile:stationsCacheFilename atomically:YES];
    NSLog(@"%@",self.detailsDict);
}

- (IBAction)details:(id)sender
{
    NSLog(@"Click");
    [self performSegueWithIdentifier:@"DetailsSegue" sender:nil];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"DetailsSegue"])
    {
        DetailStationViewController  *detailController = [segue destinationViewController];
        detailController.detailsDict = self.detailsDict;
    }
}
@end
