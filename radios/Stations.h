//
//  Stations.h
//  radios
//
//  Created by Artem on 25.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Stations : NSObject

@property (nonatomic, strong) NSDictionary *genres;
@property (nonatomic, strong) NSDictionary *stations;
@property (nonatomic,strong) NSString *radioStation;

- (void)DownloadGenreWithApiKeyWithCompletionBlock:(void(^)())completionBlock;
- (void)DowlonadStationsWithId:(NSString *)id CompletionBlock:(void(^)())completionBlock;
- (void)DownloadStationToPlayWithId:(NSString *)idStation CompletionBlock:(void(^)())completionBlock;

@end
