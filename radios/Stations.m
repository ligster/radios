//
//  Stations.m
//  radios
//
//  Created by Artem on 25.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import "Stations.h"
#import "ConnectionManager.h"

@implementation Stations

static NSString *apiKey = @"fa1669MuiRPorUBw";

- (void)DownloadGenreWithApiKeyWithCompletionBlock:(void(^)())completionBlock
{
    NSMutableString *string = [[NSMutableString alloc]initWithFormat:@"http://api.shoutcast.com/genre/primary?k=fa1669MuiRPorUBw&f=json"];
    [ConnectionManager DownloadData:string completionBlock:^(NSDictionary *responce) {
        if (responce != nil)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setObject:responce forKey:@"genre"];
            self.genres = dict;
            completionBlock();
        }
        else{
            NSLog(@"EROOR");
        }
    } ];
}

- (void)DowlonadStationsWithId:(NSString *)id CompletionBlock:(void(^)())completionBlock
{
    NSMutableString *string = [[NSMutableString alloc] initWithFormat:@"http://api.shoutcast.com/station/advancedsearch?genre_id=%@&limit=10&f=json&k=fa1669MuiRPorUBw", id];
    [ConnectionManager DownloadData:string completionBlock:^(NSDictionary *responce) {
        if (responce != nil)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:responce forKey:@"stations"];
            self.stations = dict;
            completionBlock();
        }
        else
        {
            NSLog(@"EROOR");
        }
    } ];
}

- (void)DownloadStationToPlayWithId:(NSString *)idStation CompletionBlock:(void(^)())completionBlock
{
    NSMutableString *string = [[NSMutableString alloc]initWithFormat:@"http://yp.shoutcast.com/sbin/tunein-station.pls?id=%@",idStation];
    [ConnectionManager DownloadDataToPlay:string completionBlock:^(NSData *responce) {
        NSString *newStr = [[NSString alloc] initWithData:responce encoding:NSUTF8StringEncoding];
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"(?<=File1=)(http:..[0-9]+.[0-9]+.[0-9]+.[0-9]+:[0-9]+)"
                                      options:NSRegularExpressionCaseInsensitive
                                      error:&error];
        if (error)
        {
            NSLog(@"Error creating regular expr: %@", [error localizedDescription]);
        }
        NSArray *matchs = [NSArray new];
        matchs = [regex matchesInString:newStr options:0 range:NSMakeRange(0, newStr.length)];
        for (NSTextCheckingResult *match in matchs) {
            NSString *matchText = [newStr substringWithRange:[match range]];
           // NSLog(@"match: %@", matchText);
            self.radioStation = matchText;
            completionBlock();
        }
    }];
}

@end
