//
//  StationsTableViewController.h
//  radios
//
//  Created by Artem on 25.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StationsTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activitiIndicator;
@property (nonatomic, strong) NSDictionary *genres;
@property (nonatomic, strong) NSMutableArray *genresArray;

@end
