//
//  StationsTableViewController.m
//  radios
//
//  Created by Artem on 25.03.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import "StationsTableViewController.h"
#import "Stations.h"
#import "ChooseStationsTableViewController.h"

@interface StationsTableViewController ()

@end

@implementation StationsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    Stations *stations = [Stations new];
    [self.activitiIndicator startAnimating];
    [stations DownloadGenreWithApiKeyWithCompletionBlock:^{
        self.genresArray = stations.genres[@"genre"][@"response"][@"data"][@"genrelist"][@"genre"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self.activitiIndicator stopAnimating];
        });
    }];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"StationsSegue"])
    {
        ChooseStationsTableViewController *stationsController = [segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        stationsController.id = [NSString stringWithFormat:@"%@", self.genresArray[indexPath.row][@"id"]];
    }
}


#pragma mark - @protocol UITableViewDataSource<NSObject>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
    {
        int number = [self.genresArray count];
        return number;
    }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GenreCell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:@"GenreCell"];
    }
    cell.textLabel.text = [[self.genresArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", self.genresArray[indexPath.row][@"id"]];
    return cell;
}


#pragma mark - @protocol UITableViewDelegate<NSObject, UIScrollViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"StationsSegue" sender:nil];
}

@end
